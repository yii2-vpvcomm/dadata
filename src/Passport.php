<?php
/**
 * Created at: 07.04.2018 12:42
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getNumber()
 * @method string getQc()
 * @method string getSeries()
 * @method string getSource()
 */
class Passport extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        return ['Nothing interesting here'];
    }
}