<?php
/**
 * Created at: 07.04.2018 12:40
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getBirthdate()
 * @method string getQc()
 * @method string getSource()
 */
class Birthdate extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'birthdate'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }
}