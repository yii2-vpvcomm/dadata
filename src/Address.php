<?php
/**
 * Created at: 07.04.2018 11:02
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getArea()
 * @method string getAreaFiasId()
 * @method string getAreaKladrId()
 * @method string getAreaType()
 * @method string getAreaTypeFull()
 * @method string getAreaWithType()
 * @method string getBeltwayDistance()
 * @method string getBeltwayHit()
 * @method string getBlock()
 * @method string getBlockType()
 * @method string getBlockTypeFull()
 * @method string getCapitalMarker()
 * @method string getCity()
 * @method string getCityArea()
 * @method string getCityDistrict()
 * @method string getCityDistrictFiasId()
 * @method string getCityDistrictKladrId()
 * @method string getCityDistrictType()
 * @method string getCityDistrictTypeFull()
 * @method string getCityDistrictWithType()
 * @method string getCityFiasId()
 * @method string getCityKladrId()
 * @method string getCityType()
 * @method string getCityTypeFull()
 * @method string getCityWithType()
 * @method string getCountry()
 * @method string getFiasActualityState()
 * @method string getFiasCode()
 * @method string getFiasId()
 * @method string getFiasLevel()
 * @method string getFlat()
 * @method string getFlatArea()
 * @method string getFlatPrice()
 * @method string getFlatType()
 * @method string getFlatTypeFull()
 * @method string getGeoLat()
 * @method string getGeoLon()
 * @method string getHouse()
 * @method string getHouseFiasId()
 * @method string getHouseKladrId()
 * @method string getHouseType()
 * @method string getHouseTypeFull()
 * @method string getKladrId()
 * @method string getMetro()
 * @method string getOkato()
 * @method string getOktmo()
 * @method string getPostalBox()
 * @method string getPostalCode()
 * @method string getQc()
 * @method string getQcComplete()
 * @method string getQcGeo()
 * @method string getQcHouse()
 * @method string getRegion()
 * @method string getRegionFiasId()
 * @method string getRegionKladrId()
 * @method string getRegionType()
 * @method string getRegionTypeFull()
 * @method string getRegionWithType()
 * @method string getResult()
 * @method string getSettlement()
 * @method string getSettlementFiasId()
 * @method string getSettlementKladrId()
 * @method string getSettlementType()
 * @method string getSettlementTypeFull()
 * @method string getSettlementWithType()
 * @method string getSource()
 * @method string getSquareMeterPrice()
 * @method string getStreet()
 * @method string getStreetFiasId()
 * @method string getStreetKladrId()
 * @method string getStreetType()
 * @method string getStreetTypeFull()
 * @method string getStreetWithType()
 * @method string getTaxOffice()
 * @method string getTaxOfficeLegal()
 * @method string getTimezone()
 * @method string getUnparsedParts()
 */
class Address extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'result','postal_code','country','region_fias_id','region_kladr_id','city_fias_id','city_kladr_id',
            'street_fias_id','street_kladr_id','house_fias_id','house_kladr_id','fias_id','fias_code',
            'kladr_id','okato','oktmo','geo_lat','geo_lon'
        ];
        $full_postal_address = "{$this->response['postal_code']}, {$this->response['country']}, {$this->response['result']}";
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        $result['full_postal_address'] = $full_postal_address;
        return $result;
    }

    /**
     * Coordinates for Yandex/Google Maps
     * @return string
     */
    public function getCoordinates()
    {
        $this->coordinates = $this->getGeoLat().",".$this->getGeoLon();
        return $this->coordinates;
    }
}