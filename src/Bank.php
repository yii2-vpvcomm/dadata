<?php
/**
 * Created at: 21.04.2018 8:27
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

/**
 * @method array getAddress()
 * @method string getBic()
 * @method string getCorrespondentAccount()
 * @method array getName()
 * @method string getOkpo()
 * @method array getOpf()
 * @method string getPhone()
 * @method string getRegistrationNumber()
 * @method array getRkc()
 * @method array getState()
 * @method string getSwift()
 */

namespace vpvcomm\dadata\src;

/**
 * Suggestions for banks
 *
 * @package vpvcomm\dadata\src
 */
class Bank extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'bic','swift','okpo','correspondent_account','registration_number','phone'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }
}

/*
{
    "opf": {
        "type": "BANK",
        "full": "Сбербанк России",
        "short": "СБ"
    },
    "name": {
        "payment": "ПАО СБЕРБАНК",
        "full": "ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО \"СБЕРБАНК РОССИИ\"",
        "short": "ПАО СБЕРБАНК"
    },
    "bic": "044525225",
    "swift": "SABRRUMMXXX",
    "okpo": "00032537",
    "correspondent_account": "30101810400000000225",
    "registration_number": "1481",
    "rkc": {
        "opf": {
            "type": "RKC",
            "full": "Главное управление/Отделение (НБ)",
            "short": "ГУ/О"
        },
        "name": {
            "payment": "ГУ БАНКА РОССИИ ПО ЦФО",
            "full": null,
            "short": null
        },
        "bic": "044525000",
        "swift": null,
        "okpo": "09201220",
        "correspondent_account": null,
        "registration_number": null,
        "rkc": null,
        "address": {
            "value": "115035, МОСКВА 35, УЛ.БАЛЧУГ,2",
            "unrestricted_value": "115035, МОСКВА 35, УЛ.БАЛЧУГ,2",
            "data": null
        },
        "phone": null,
        "state": {
            "status": "ACTIVE",
            "actuality_date": 1455580800000,
            "registration_date": 889488000000,
            "liquidation_date": null
        }
    },
    "address": {
        "value": "г Москва, ул Вавилова, д 19",
        "unrestricted_value": "г Москва, ул Вавилова, д 19",
        "data": { ... }
    },
    "phone": null,
    "state": {
        "status": "ACTIVE",
        "actuality_date": 1455580800000,
        "registration_date": 844387200000,
        "liquidation_date": null
    }
}
 * */