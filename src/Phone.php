<?php
/**
 * Created at: 07.04.2018 12:42
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getCityCode()
 * @method string getCountryCode()
 * @method string getExtension()
 * @method string getNumber()
 * @method string getPhone()
 * @method string getProvider()
 * @method string getQc()
 * @method string getQcConflict()
 * @method string getRegion()
 * @method string getSource()
 * @method string getTimezone()
 * @method string getType()
 */
class Phone extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'type','phone','country_code','city_code','city_code','number','provider','region'
        ];
        $provider = $this->response['provider'];
        $this->response['provider'] = str_replace('\\','',$provider);
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }
}