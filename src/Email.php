<?php
/**
 * Created at: 07.04.2018 12:41
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getEmail()
 * @method string getQc()
 * @method string getSource()
 */
class Email extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'email'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }
}