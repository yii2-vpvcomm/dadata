<?php
/**
 * Created at: 21.04.2018 8:26
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

/**
 * @method array getAddress()
 * @method string getAuthorities()
 * @method string getBranchCount()
 * @method string getBranchType()
 * @method string getCapital()
 * @method string getDocuments()
 * @method string getEmails()
 * @method string getHid()
 * @method string getInn()
 * @method string getKpp()
 * @method string getLicenses()
 * @method array getManagement()
 * @method array getName()
 * @method string getOgrn()
 * @method string getOgrnDate()
 * @method string getOkpo()
 * @method string getOkved()
 * @method string getOkvedType()
 * @method string getOkveds()
 * @method array getOpf()
 * @method string getPhones()
 * @method string getQc()
 * @method string getSource()
 * @method array getState()
 * @method string getType()
 */

namespace vpvcomm\dadata\src;

/**
 * Suggestions for organizations
 *
 * @package vpvcomm\dadata\src
 */
class Party extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        $importantKeys = [
            'kpp','inn','ogrn','okpo','okved','okveds','hid','phones','emails','ogrn_date','okved_type'
        ];
        $result = array_intersect_key($this->response,$this->addValue($importantKeys));
        return $result;
    }

    /**
     * Get organization name with full opf
     * @return mixed
     */
    public function getNameFullWithOpf()
    {
        try {
            $data = $this->getSuggestData();
            return $data['name']['full_with_opf'];
        } catch (\Exception $exception) {
            return [];
        }
    }
}

/*
{
    "kpp": "773601001",
    "capital": null,
    "management": {
        "name": "Греф Герман Оскарович",
        "post": "Президент, председатель правления"
    },
    "branch_type": "MAIN",
    "branch_count": 93,
    "source": null,
    "qc": null,
    "hid": "b347aa7ff5df1158063c052ca6c8c283c942d03a4d8b2ca5168899b26a9c246d",
    "type": "LEGAL",
    "state": {
        "status": "ACTIVE",
        "actuality_date": 1506988800000,
        "registration_date": 677376000000,
        "liquidation_date": null
    },
    "opf": {
        "type": null,
        "code": "12247",
        "full": "Публичное акционерное общество",
        "short": "ПАО"
    },
    "name": {
        "full_with_opf": "ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО \"СБЕРБАНК РОССИИ\"",
        "short_with_opf": "ПАО СБЕРБАНК",
        "latin": null,
        "full": "СБЕРБАНК РОССИИ",
        "short": "СБЕРБАНК"
    },
    "inn": "7707083893",
    "ogrn": "1027700132195",
    "okpo": null,
    "okved": "64.19",
    "okveds": null,
    "authorities": null,
    "documents": null,
    "licenses": null,
    "address": {
        "value": "г Москва, ул Вавилова, д 19",
        "unrestricted_value": "г Москва, Академический р-н, ул Вавилова, д 19",
        "data": { ... }
    },
    "phones": null,
    "emails": null,
    "ogrn_date": 1029456000000,
    "okved_type": "2014"
}
 * */