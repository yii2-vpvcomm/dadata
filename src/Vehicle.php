<?php
/**
 * Created at: 07.04.2018 12:43
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

/**
 * @method string getBrand()
 * @method string getModel()
 * @method string getQc()
 * @method string getResult()
 * @method string getSource()
 */
class Vehicle extends AbstractParent
{
    /**
     * Get important values from dadata service response
     * @return array
     */
    public function getImportantValues()
    {
        return ['Nothing interesting here'];
    }
}