<?php
/**
 * Created at: 07.04.2018 11:00
 * @author vpvcomm <vpvcomm@gmail.com>
 * @link http://vpvcomm.ru/
 * @copyright Copyright (c) 2018 vpvcomm
 */

namespace vpvcomm\dadata\src;

use vpvcomm\dadata\ExampleTrait;

/**
 * Parent class for the family of classes: address, bank, name, etc.
 *
 * @package vpvcomm\dadata\src
 */
abstract class AbstractParent
{
    use ExampleTrait;

    protected $token;
    protected $secret;
    protected $url = 'https://dadata.ru/api/v2/clean';

    protected $suggestUrl = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest';
    protected $suggestAllowed = ['bank','party'];
    protected $suggestIs = false;

    protected $data;
    protected $structure;
    protected $response;

    protected $className;
    protected $notEmpty;
    protected $coordinates;

    /**
     * Return important values from dadata response
     * @return mixed
     */
    abstract public function getImportantValues();

    /**
     * AbstractParent constructor.
     * @param array $config
     * @param bool $suggest
     */
    public function __construct(array $config, $suggest = false)
    {
        $currentClass = strtolower($this->getClassName());
        $this->token = $config['token'];
        $this->secret = $config['secret'];
        $this->url .= '/'.$currentClass;
        $this->suggestIs = $suggest;
        $this->structure = $this->getStructure();
        if ($suggest && in_array($currentClass,$this->suggestAllowed)) {
            $this->url = $this->suggestUrl.'/'.$currentClass;
        }
    }

    /**
     * Magic method to call getter method
     * based on response key
     * from dadata.ru
     * @param $name
     * @param array $params
     * @return string
     */
    public function __call($name,array $params)
    {
        $method = substr($name,3);
        $key = strtolower(
            preg_replace('/(.)([A-Z])/', "$1_$2", $method)
        );
        if (array_key_exists($key,$this->response)) {
            return $this->response[$key];
        }
        return "You tried to call the method: $name()";
    }

    /**
     * Get suggestions data response
     * @return array
     */
    protected function getSuggestData($data)
    {
        try {
            if ($this->suggestIs) {
                return $data['suggestions'][0]['data'];
            }
        } catch (\Exception $exception) {
            return [];
        }
    }

    /**
     * Initialize CURLOPT_HTTPHEADER
     * @return array
     */
    protected function curlOptHeaders()
    {
        $headers = [
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Token ' . $this->token
        ];
        if (!$this->suggestIs) {
            $headers[] = 'X-Secret: ' . $this->secret;
        }
        return $headers;
    }

    /**
     * Request to dadata.ru service via CURL
     * @param array $data
     * @return $this|array
     */
    public function request(array $data)
    {
        $prepareData = $this->prepareData($data);
        if ($curl = curl_init($this->url)) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->curlOptHeaders());
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($prepareData));
            $result = curl_exec($curl);
            $this->response = $this->prepareResponse($result);
            curl_close($curl);
            return $this;
        } else {
            return ['Something went wrong! In '.__CLASS__];
        }
    }

    /**
     * Get non-empty values from dadata service response array
     * @return $this
     */
    public function getNotEmpty()
    {
        foreach ($this->response as $key => $val) {
            if (!empty($val)) {
                $this->notEmpty[$key] = $val;
            }
        }
        return $this->notEmpty;
    }

    /**
     * Add values to important keys
     * to get array_intersect_key differences
     * @return array
     */
    protected function addValue(array $importantKeys)
    {
        $result = [];
        foreach ($importantKeys as $item) {
            $result[$item] = true;
        }
        return $result;
    }

    /**
     * Get child class name from full class path
     * @return mixed|string
     */
    protected static function getClassName()
    {
        $class = get_called_class();
        $class = str_replace(__NAMESPACE__.'\\','',$class);
        return $class;
    }

    /**
     * Prepare structure field for request to dadata.ru service
     * @return mixed|string
     */
    protected function getStructure()
    {
        $this->structure = strtoupper(self::getClassName());
        return $this->structure;
    }

    /**
     * Get prepared dadata service response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Display example response for current class
     * @return string
     */
    public static function getExample()
    {
        $class = ucfirst(self::getClassName());
        $example = 'example'.$class;
        $result = "<h3>Пример ответа для класса {$class}</h3>";
        $result .= '<pre>'.self::$$example.'</pre>';
        return $result;
    }

    /**
     * Prepare the user data for request to dadata service
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data)
    {
        $prepared = $data;
        if (!$this->suggestIs) {
            $prepared = [
                "structure" => [$this->structure],
                "data" => [$data]
            ];
        }
        return $prepared;
    }

    /**
     * Prepare dadata service response for further use
     * @param $response
     * @return mixed
     */
    protected function prepareResponse($response)
    {
        $data = json_decode($response, true);
        $prepared = $this->suggestIs ? $this->getSuggestData($data) : $data[0];
        return $prepared;
    }

}